<?php
namespace CMDProcessor\CacheCleaner\Api;
 
interface CacheCleanerInterface {
    
    /**
     * Clears and flush all magento cache
     *
     * @api
     * @return boolean
     */
    public function complete();

    /**
     * Clears all magento cache
     *
     * @api
     * @return boolean
     */
    public function clean();

    /**
     * Clears a specific type of cache.
     * 
     * @api
     * @param string $cache
     * @return boolean
     */
    public function cleanSpecific($cache);

	/**
     * Flushes all cache.
     * 
     * @api
     * @return boolean
     */
    public function flush();

    /**
     * Flushes a specific type of cache.
     * 
     * @api
     * @param string $cache
     * @return boolean
     */
    public function flushSpecific($cache);
}