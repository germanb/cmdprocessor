<?php
namespace CMDProcessor\CacheCleaner\Model;
 
use CMDProcessor\CacheCleaner\Api\CacheCleanerInterface as ApiInterface;
 
class CacheCleaner implements ApiInterface {
    /**
     * Clears and flush all magento cache
     *
     * @api
     * @return boolean
     */
    public function complete() {
        try {
            shell_exec('php /var/www/html/magento2/bin/magento cache:clean');
            $result = shell_exec('php /var/www/html/magento2/bin/magento cache:flush');
            return $result;
        } catch (\ErrorException $e) {
            return $e
        }
    }

    /**
     * Clears all magento cache
     *
     * @api
     * @return boolean
     */
    public function clean() {
        try {
            $result = shell_exec('php /var/www/html/magento2/bin/magento cache:clean');
            return $result;
        } catch (\ErrorException $e) {
            return $e
        }
    }

    /**
     * Clears a specific type of cache.
     * 
     * @api
     * @param string $cache
     * @return boolean
     */
    public function cleanSpecific($cache='') {
        try {
            $result = shell_exec('php /var/www/html/magento2/bin/magento cache:clean '.$cache);
            return $result;
        } catch (\ErrorException $e) {
            return $e
        }
    }

    /**
     * Flushes all cache.
     * 
     * @api
     * @return boolean
     */
    public function flush() {
        try {
            $result = shell_exec('php /var/www/html/magento2/bin/magento cache:flush');
            return $result;
        } catch (\ErrorException $e) {
            return $e
        }
    }

    /**
     * Flushes a specific type of cache.
     * 
     * @api
     * @param string $cache
     * @return boolean
     */
    public function flushSpecific($cache='') {
        try {
            $result = shell_exec('php /var/www/html/magento2/bin/magento cache:flush '.$cache);
            return $result;
        } catch (\ErrorException $e) {
            return $e
        }
    }
}