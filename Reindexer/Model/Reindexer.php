<?php
namespace CMDProcessor\Reindexer\Model;
 
use CMDProcessor\Reindexer\Api\ReindexerInterface as ApiInterface;
 
class Reindexer implements ApiInterface {

    /**
     * Resets and reindex all.
     *
     * @api
     * @return boolean
     */
    public function reindex() {
        try {
            $result = shell_exec('php /var/www/html/magento2/bin/magento indexer:reindex');
            return $result;
        } catch (\ErrorException $e) {
            return $e
        }
    }

    /**
     * Resets and reindex a specific index.
     *
     * @api
     * @param string $index 
     * @return boolean
     */
    public function reindexSpecific($index) {
        try {
            $result = shell_exec('php /var/www/html/magento2/bin/magento indexer:reindex '.$index);
            return $result;
        } catch (\ErrorException $e) {
            return $e
        }
    }

    /**
     * Resets all.
     *
     * @api
     * @return boolean
     */
    public function reset() {
        try {
            $result = shell_exec('php /var/www/html/magento2/bin/magento indexer:reset');
            return $result;
        } catch (\ErrorException $e) {
            return $e
        }
    }

    /**
     * Resets a specifir index.
     *
     * @api
     * @param string $index 
     * @return boolean
     */
    public function resetSpecific($index) {
        try {
            $result = shell_exec('php /var/www/html/magento2/bin/magento indexer:reset '.$index);
            return $result;
        } catch (\ErrorException $e) {
            return $e
        }
    }
}