<?php
namespace CMDProcessor\Reindexer\Api;
 
interface ReindexerInterface {
    /**
     * Resets and reindex all.
     *
     * @api
     * @return boolean
     */
    public function reindex();

    /**
     * Resets and reindex a specific index.
     *
     * @api
     * @param string $index 
     * @return boolean
     */
    public function reindexSpecific($index);

    /**
     * Resets all.
     *
     * @api
     * @return boolean
     */
    public function reset();

    /**
     * Resets a specifir index.
     *
     * @api
     * @param string $index 
     * @return boolean
     */
    public function resetSpecific($index);
}